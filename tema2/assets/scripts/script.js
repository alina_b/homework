document.querySelector('#parse').addEventListener('click',()=>{
    const fileSelector = document.querySelector('#file');
    if (fileSelector.files.length > 0){
        const file = fileSelector.files[0]
        const reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = (evt) => {
           console.log(evt.target.result);
        }
        reader.onerror = (evt) => {
            console.error("error reading file");
        }
        
    }else{
        alert("No file uploaded")
    }
   
})